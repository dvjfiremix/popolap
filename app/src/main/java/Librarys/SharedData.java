package Librarys;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import static Librarys.Globales.SHARED_APP;

public class SharedData extends AppCompatActivity {
    private static SharedData sharedData;
    private static String cadena = "";
    private static boolean data = false;
    private static int numero = 0;
    private SharedPreferences sharedPreferences;

    private SharedData(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_APP, Context.MODE_PRIVATE);
    }

    public static SharedData getInstance(Context context) {
        if (context != null) {
            sharedData = new SharedData(context);
        }
        return sharedData;
    }

    public boolean SaveShared(int op, String key, String value, int dato, boolean estado) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            switch (op) {

                //Dato de texto
                case 0:
                    editor.putString(key, value);
                    break;

                //Dato booleano
                case 1:
                    editor.putBoolean(key, estado);
                    break;

                //Dato numerico
                case 2:
                    editor.putInt(key, dato);
                    break;
            }
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean DestroyerData() {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //Obtiene un valor de tipo booleano de las preferencias
    public Boolean getBoolean(String key) {
        try {
            if (sharedPreferences != null) {
                data = sharedPreferences.getBoolean(key, false);
            }
        } catch (Exception e) {
            return data;
        }
        return data;
    }

    //Obtiene un valor de tipo String de las preferencias
    public String getString(String key, String value) {
        try {
            if (sharedPreferences != null) {
                cadena = sharedPreferences.getString(key, value);
            }
        } catch (Exception e) {
            return cadena;
        }
        return cadena;
    }

    //Obtiene un valor de tipo int de las preferencias
    public int getInt(String key, int value) {
        try {
            if (sharedPreferences != null) {
                numero = sharedPreferences.getInt(key, value);
            }
        } catch (Exception e) {
            return numero;
        }
        return numero;
    }

}
