package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.srll.app.itssat.popolucaapp.Utils.Utils;
import com.srll.app.itssat.popolucaapp.rompecabezas.ImageAdapter2;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;

public class MenuRompeCabezasActivity2 extends AppCompatActivity {

    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_rompe_cabeza2);

        AssetManager am = getAssets();
        try {
            final String[] files = am.list("img2");

            GridView grid = findViewById(R.id.grid2);
            grid.setAdapter(new ImageAdapter2(this));
            grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getApplicationContext(), RompecabezasActivity2.class);
                    intent.putExtra("assetName", files[i % files.length]);
                    startActivity(intent);
                }
            });
        } catch (IOException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MenuVocales.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }
}
