package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.srll.app.itssat.popolucaapp.Sliders.Intro;
import com.srll.app.itssat.popolucaapp.Sliders.IntroCortado;
import com.srll.app.itssat.popolucaapp.Sliders.IntroLargo;
import com.srll.app.itssat.popolucaapp.Sliders.IntroRearticulado;
import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class MenuIntro extends AppCompatActivity {
    ImageView intronormal, introcortado, introlargo, introrearticulado;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_intro);

        intronormal = findViewById(R.id.imagenNormal);
        introcortado = findViewById(R.id.imagenCortado);
        introlargo = findViewById(R.id.imagenLargo);
        introrearticulado = findViewById(R.id.imagenRearticulado);

        intronormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Intro.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        introcortado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), IntroCortado.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        introlargo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), IntroLargo.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        introrearticulado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), IntroRearticulado.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });
    }
}
