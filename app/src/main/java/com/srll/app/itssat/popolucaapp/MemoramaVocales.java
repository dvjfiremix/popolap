package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class MemoramaVocales extends AppCompatActivity implements View.OnClickListener {
    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, imgCapturada;
    Button jugar, salir;
    TextView idMensaje;
    int cap1, cap2, control1, control2, contador;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memorama_vocales);

        img1 = findViewById(R.id.imageView1);
        img2 = findViewById(R.id.imageView2);
        img3 = findViewById(R.id.imageView3);
        img4 = findViewById(R.id.imageView4);
        img5 = findViewById(R.id.imageView5);
        img6 = findViewById(R.id.imageView6);
        img7 = findViewById(R.id.imageView7);
        img8 = findViewById(R.id.imageView8);
        img9 = findViewById(R.id.imageView9);
        img10 = findViewById(R.id.imageView10);
        img11 = findViewById(R.id.imageView11);
        img12 = findViewById(R.id.imageView12);

        jugar = findViewById(R.id.jugar);
        salir = findViewById(R.id.salir);

        idMensaje = findViewById(R.id.contador);

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        img5.setOnClickListener(this);
        img6.setOnClickListener(this);
        img7.setOnClickListener(this);
        img8.setOnClickListener(this);
        img9.setOnClickListener(this);
        img10.setOnClickListener(this);
        img11.setOnClickListener(this);
        img12.setOnClickListener(this);
        jugar.setOnClickListener(this);
        salir.setOnClickListener(this);

        disable();
        contador = 0;
    }

    // desabilitamos loa botnoes para poder activarlos despues
    public void disable() {
        img1.setEnabled(false);
        img2.setEnabled(false);
        img3.setEnabled(false);
        img4.setEnabled(false);
        img5.setEnabled(false);
        img6.setEnabled(false);
        img7.setEnabled(false);
        img8.setEnabled(false);
        img9.setEnabled(false);
        img10.setEnabled(false);
        img11.setEnabled(false);
        img12.setEnabled(false);
    }

    //Asignamos por defecto los parametros del memorama y los ponemos disponibles
    public void enable() {

        img1.setImageResource(R.drawable.logo);
        img2.setImageResource(R.drawable.logo);
        img3.setImageResource(R.drawable.logo);
        img4.setImageResource(R.drawable.logo);
        img5.setImageResource(R.drawable.logo);
        img6.setImageResource(R.drawable.logo);
        img7.setImageResource(R.drawable.logo);
        img8.setImageResource(R.drawable.logo);
        img9.setImageResource(R.drawable.logo);
        img10.setImageResource(R.drawable.logo);
        img11.setImageResource(R.drawable.logo);
        img12.setImageResource(R.drawable.logo);
        img1.setEnabled(true);
        img2.setEnabled(true);
        img3.setEnabled(true);
        img4.setEnabled(true);
        img5.setEnabled(true);
        img6.setEnabled(true);
        img7.setEnabled(true);
        img8.setEnabled(true);
        img9.setEnabled(true);
        img10.setEnabled(true);
        img11.setEnabled(true);
        img12.setEnabled(true);

    }

    // Comparamnos si es el par de la imagen para marcar como acertado
    public void compare(int idCurrentImage, int idBox, int idCurrentBox, final ImageView imgBox) {
        if (cap1 == 0) {//primera vez
            cap1 = idCurrentImage;
            control1 = idBox;
            imgCapturada = findViewById(control1);
        } else {//segunda vez
            if (control1 != idBox) {//cajas distintas
                cap2 = idCurrentImage;
                if (cap2 != cap1) {//imagenes distintas
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imgCapturada.setImageResource(R.drawable.logo);
                            imgBox.setImageResource(R.drawable.logo);
                        }
                    }, 100);
                } else {//las imagenes son iguales
                    imgCapturada.setEnabled(false);
                    imgBox.setEnabled(false);
                    contador++;
                }
                cap2 = 0;
                cap1 = 0;
            } else {//los controles son iguales
                cap2 = 0; //para escoger otro control
            }
        }
    }

    //Creamos un cronometro para validar si terminas el memorama en el tiempo indicado
    public void cronometer() {
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String seconds = String.valueOf(millisUntilFinished / 1000);
                jugar.setEnabled(false);
                idMensaje.setText(seconds);
                if (contador == 6) {
                    idMensaje.setText("¡¡Ganaste!!");
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                disable();
                jugar.setEnabled(true);
                if (contador != 6) {
                    idMensaje.setText("¡¡Perdiste!!");
                }
                this.cancel();
            }
        }.start();
    }

    // Evento clic de los botones memorama, iniciar y salir
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView1:
                img1.setImageResource(R.drawable.imagen1);
                compare(R.drawable.imagen1, R.id.imageView1, v.getId(), img1);
                break;
            case R.id.imageView2:
                img2.setImageResource(R.drawable.imagen2);
                compare(R.drawable.imagen2, R.id.imageView2, v.getId(), img2);
                break;
            case R.id.imageView3:
                img3.setImageResource(R.drawable.imagen3);
                compare(R.drawable.imagen3, R.id.imageView3, v.getId(), img3);
                break;
            case R.id.imageView4:
                img4.setImageResource(R.drawable.imagen4);
                compare(R.drawable.imagen4, R.id.imageView4, v.getId(), img4);
                break;
            case R.id.imageView5:
                img5.setImageResource(R.drawable.imagen5);
                compare(R.drawable.imagen5, R.id.imageView5, v.getId(), img5);
                break;
            case R.id.imageView6:
                img6.setImageResource(R.drawable.imagen6);
                compare(R.drawable.imagen6, R.id.imageView6, v.getId(), img6);
                break;
            case R.id.imageView7:
                img7.setImageResource(R.drawable.imagen1);
                compare(R.drawable.imagen1, R.id.imageView7, v.getId(), img7);
                break;
            case R.id.imageView8:
                img8.setImageResource(R.drawable.imagen2);
                compare(R.drawable.imagen2, R.id.imageView8, v.getId(), img8);
                break;
            case R.id.imageView9:
                img9.setImageResource(R.drawable.imagen3);
                compare(R.drawable.imagen3, R.id.imageView9, v.getId(), img9);
                break;
            case R.id.imageView10:
                img10.setImageResource(R.drawable.imagen4);
                compare(R.drawable.imagen4, R.id.imageView10, v.getId(), img10);
                break;
            case R.id.imageView11:
                img11.setImageResource(R.drawable.imagen5);
                compare(R.drawable.imagen5, R.id.imageView11, v.getId(), img11);
                break;
            case R.id.imageView12:
                img12.setImageResource(R.drawable.imagen6);
                compare(R.drawable.imagen6, R.id.imageView12, v.getId(), img12);
                break;
            case R.id.jugar:
                enable();
                contador = 0;
                cronometer();
                break;
            case R.id.salir:
                finish();
                break;
        }
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MenuVocales.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }

}
