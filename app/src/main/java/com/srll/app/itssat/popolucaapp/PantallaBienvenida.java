package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class PantallaBienvenida extends AppCompatActivity {

    MediaPlayer mediaPlayer;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_bienvenida);

        mediaPlayer = MediaPlayer.create(this, R.raw.bienvenida);
        mediaPlayer.start();

    }

    public void Siguiente(View view) {
        Intent siguiente = new Intent(this, MainActivity.class);
        startActivity(siguiente);
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        finish();
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MainActivity.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }

}
