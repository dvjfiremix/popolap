package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class MenuMemoramaVocales extends AppCompatActivity {
    ImageView normal, cortado, largo, rearticulado;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_memorama_vocales);

        normal = findViewById(R.id.imagenNormal);
        cortado = findViewById(R.id.imagenCortado);
        largo = findViewById(R.id.imagenLargo);
        rearticulado = findViewById(R.id.imagenRearticulado);

        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MemoramaVocales.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        cortado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MemoramaCortado.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });
        largo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MemoramaLargo.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        rearticulado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MemoramaRearticulado.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MenuVocales.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }


}
