package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.os.Bundle;

import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;

public class MenuSaludos extends AppCompatActivity {

    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_saludos);
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MainActivity.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }
}
