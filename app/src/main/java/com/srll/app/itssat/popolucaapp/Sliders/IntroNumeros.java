package com.srll.app.itssat.popolucaapp.Sliders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srll.app.itssat.popolucaapp.MenuNumeros;
import com.srll.app.itssat.popolucaapp.R;
import com.srll.app.itssat.popolucaapp.Sliders.Fragments.FragmentNumeros;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class IntroNumeros extends AppCompatActivity {
    ViewPagerAdapter myViewPagerAdapter;
    TextView[] dots;
    ViewPager viewPagers;
    LinearLayout dotsLayout;
    Button anterior, siguiente;

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            try {
                addBottomDots(position);
                if (position == 0) {
                    siguiente.setText(getString(R.string.seguir));
                    siguiente.setVisibility(View.VISIBLE);
                    anterior.setVisibility(View.GONE);
                } else if (position == 6) {
                    siguiente.setText(getString(R.string.salir));
                } else {
                    siguiente.setText(getString(R.string.seguir));
                    anterior.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
        anterior = findViewById(R.id.anterior);
        siguiente = findViewById(R.id.siguiente);
        viewPagers = findViewById(R.id.viewPager);
        dotsLayout = findViewById(R.id.layoutDots);

        setupViewPager();
        addBottomDots(0);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < myViewPagerAdapter.getCount()) {
                    viewPagers.setCurrentItem(current);
                } else {
                    cargar();
                }
            }
        });

        anterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current < myViewPagerAdapter.getCount()) {
                    viewPagers.setCurrentItem(current);
                } else {
                    cargar();
                }
            }
        });
    }

    public void cargar() {
        Intent n = new Intent(getApplicationContext(), MenuNumeros.class);
        n.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
    }

    private void addBottomDots(int currentPage) {
        try {
            dots = new TextView[myViewPagerAdapter.getCount()];
            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[currentPage]);
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(colorsActive[currentPage]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager() {
        myViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        myViewPagerAdapter.addFragment(new FragmentNumeros("Uno", "tuum", R.drawable.alfabeto, R.raw.a), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Dos", "wɨsteen", R.drawable.colores, R.raw.e), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Tres", "tukuteen", R.drawable.cuerpo, R.raw.i), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Cuatro", "maktɑsteen", R.drawable.saludos, R.raw.u), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Cinco", "mosteen", R.drawable.familia, R.raw.ii), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Seis", "mosnɑtuum", R.drawable.frutas, R.raw.o), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Siete", "mosnɑwɨsteen", R.drawable.saludos, R.raw.u), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Ocho", "mosnɑtukuteen", R.drawable.frutas, R.raw.o), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Nueve", "mosnɑmɑktasteen", R.drawable.saludos, R.raw.u), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Diez", "mɑknɑ'", R.drawable.frutas, R.raw.o), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Veinte", "wɨsnɑmɑknɑ'", R.drawable.saludos, R.raw.u), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Treintɑ", "tukunɑmɑknɑ'", R.drawable.frutas, R.raw.o), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Cuarentɑ", "mɑktɑsteenmaknɑ'", R.drawable.saludos, R.raw.u), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentNumeros("Cincuentɑ", "mosnɑmɑknɑ'", R.drawable.saludos, R.raw.u), "Exposición");
        viewPagers.setAdapter(myViewPagerAdapter);
        viewPagers.addOnPageChangeListener(pageChangeListener);
    }

    private int getItem(int i) {
        return viewPagers.getCurrentItem() + i;
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
