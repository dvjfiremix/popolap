package com.srll.app.itssat.popolucaapp.Services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.srll.app.itssat.popolucaapp.R;

import Librarys.SharedData;
import androidx.annotation.Nullable;

import static Librarys.Globales.AUDIO;

public class SoundsPlay extends Service {
    boolean stop = false;
    int Musica;
    private MediaPlayer mediaPlayer;
    private SharedData sharedData;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedData = SharedData.getInstance(this);
        Musica = sharedData.getInt(AUDIO, R.raw.animales);
        //Start media player
        mediaPlayer = MediaPlayer.create(this, Musica);
        mediaPlayer.start();
        mediaPlayer.setLooping(false);//set looping true to run it infinitely*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //On destory stop and release the media player
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();
            stop = true;
            sharedData.DestroyerData();
        }
    }
}
