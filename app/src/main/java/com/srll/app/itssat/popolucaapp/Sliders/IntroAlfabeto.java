package com.srll.app.itssat.popolucaapp.Sliders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srll.app.itssat.popolucaapp.MenuAlfabeto;
import com.srll.app.itssat.popolucaapp.R;
import com.srll.app.itssat.popolucaapp.Services.SoundsPlay;
import com.srll.app.itssat.popolucaapp.Sliders.Fragments.FragmentAlfabeto;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class IntroAlfabeto extends AppCompatActivity {
    ViewPagerAdapter myViewPagerAdapter;
    TextView[] dots;
    ViewPager viewPagers;
    LinearLayout dotsLayout;
    Button anterior, siguiente;

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            try {
                addBottomDots(position);
                if (position == 0) {
                    siguiente.setText(getString(R.string.seguir));
                    siguiente.setVisibility(View.VISIBLE);
                    anterior.setVisibility(View.GONE);
                } else if (position == 26) {
                    siguiente.setText(getString(R.string.salir));
                } else {
                    siguiente.setText(getString(R.string.seguir));
                    anterior.setVisibility(View.VISIBLE);
                }
                stopService(new Intent(IntroAlfabeto.this, SoundsPlay.class));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
        anterior = findViewById(R.id.anterior);
        siguiente = findViewById(R.id.siguiente);
        viewPagers = findViewById(R.id.viewPager);


        setupViewPager();
        addBottomDots(0);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < myViewPagerAdapter.getCount()) {
                    viewPagers.setCurrentItem(current);
                } else {
                    cargar();
                }
                stopService(new Intent(IntroAlfabeto.this, SoundsPlay.class));
            }
        });

        anterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current < myViewPagerAdapter.getCount()) {
                    viewPagers.setCurrentItem(current);
                } else {
                    cargar();
                }
                stopService(new Intent(IntroAlfabeto.this, SoundsPlay.class));
            }
        });
    }

    public void cargar() {
        Intent n = new Intent(getApplicationContext(), MenuAlfabeto.class);
        n.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
    }

    private void addBottomDots(int currentPage) {
        try {
            dots = new TextView[myViewPagerAdapter.getCount()];
            int[] colorsActive = getResources().getIntArray(R.array.array_active_Alfabeto);
            int[] colorsInactive = getResources().getIntArray(R.array.array_inactive_Alfabeto);

            dotsLayout.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[currentPage]);
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(colorsActive[currentPage]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager() {
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Tortillɑ", "ɑɑñi'", R.drawable.tortilla, R.raw.tortilla), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑbezɑ", "koobak", R.drawable.cabeza, R.raw.cabeza), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Tomɑte", "chiipiñ", R.drawable.tomate, R.raw.tomate), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("No", "dya'", R.drawable.no, R.raw.no), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Anciɑno", "wɨdyɑɑyɑ'", R.drawable.anciano, R.raw.anciano), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑngrejo", "eexe'", R.drawable.cangrejo, R.raw.cangrejo), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Comɑl", "---", R.drawable.comal, R.raw.comal), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Ojo", "ixkuy", R.drawable.ojo, R.raw.ojo), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Mɑiz en grɑno", "ɨkxi'", R.drawable.maiz, R.raw.maizgrano), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Sol", "Jaama'", R.drawable.sol, R.raw.sol), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Leñɑ", "kɨɨpi'", R.drawable.lena, R.raw.lena), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Gelatinoso", "lɨ'nkɨy", R.drawable.gelatina, R.raw.gelatinoso), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Flor", "mooyɑ'", R.drawable.flor, R.raw.flor), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Sɑpo", "nɑk", R.drawable.sapo, R.raw.sapo), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Chile", "ñiiwi'", R.drawable.chile, R.raw.chile), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑnɑstɑ", "----", R.drawable.canasta, R.raw.canasta), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑñɑ de otɑte ", "ojwiñ", R.drawable.cana, R.raw.cana), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Lɑgɑrtijɑ", "pɑɑchi'", R.drawable.lagartija, R.raw.lagartija), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Rɑcimo", "----", R.drawable.racimo, R.raw.racimo), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑrɑcol", "Suutyi'", R.drawable.caracol, R.raw.caracol), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑsɑ", "tɨk", R.drawable.casa, R.raw.casa), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑmɑ", "tse'es", R.drawable.cama, R.raw.cama), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Tortugɑ", "tyuuki'", R.drawable.tortuga, R.raw.tortuga), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Piñɑ", "uuju'", R.drawable.pina, R.raw.pina), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cɑbello", "wɑy", R.drawable.cabello, R.raw.cabello), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Nido", "xeket", R.drawable.nido, R.raw.nido), "Exposición");
        myViewPagerAdapter.addFragment(new FragmentAlfabeto("Cerdo", "yooyɑ'", R.drawable.cerdo, R.raw.cerdo), "Exposición");
        viewPagers.setAdapter(myViewPagerAdapter);
        viewPagers.addOnPageChangeListener(pageChangeListener);
    }

    private int getItem(int i) {
        return viewPagers.getCurrentItem() + i;
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(IntroAlfabeto.this, SoundsPlay.class));
        super.onDestroy();
    }

    @Override
    public void onPause() {
        stopService(new Intent(IntroAlfabeto.this, SoundsPlay.class));
        super.onPause();
    }
}
