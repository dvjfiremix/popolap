package com.srll.app.itssat.popolucaapp.Utils;

import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Utils extends AppCompatActivity {
    private static Utils utils;
    private Context context;

    private Utils(Context context) {
        this.context = context;
    }

    public static Utils getInstance(Context context) {
        if (context != null) {
            utils = new Utils(context);
        }
        return utils;
    }

    public void Regresar() {
        Toast.makeText(context.getApplicationContext(), "Regresando al menu", Toast.LENGTH_SHORT).show();
    }
}
