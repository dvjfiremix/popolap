package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    MediaPlayer mediaPlayer;
    //Declarar los ImageView
    Button play;
    ImageView vocales, numeros, frutas, palabras, alfabeto, colores, animales, saludos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicalizamos los ImageView y le asignamos su ID
        vocales = findViewById(R.id.vocales);
        numeros = findViewById(R.id.numeros);
        frutas = findViewById(R.id.frutas);
        palabras = findViewById(R.id.palabras);
        alfabeto = findViewById(R.id.alfabeto);
        colores = findViewById(R.id.colores);
        animales = findViewById(R.id.animales);
        saludos = findViewById(R.id.saludos);

        //ClickListener
        vocales.setOnClickListener(this);
        numeros.setOnClickListener(this);
        frutas.setOnClickListener(this);
        palabras.setOnClickListener(this);
        alfabeto.setOnClickListener(this);
        animales.setOnClickListener(this);
        saludos.setOnClickListener(this);
        colores.setOnClickListener(this);

    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.salir:
                int id = item.getItemId();
                if (id == R.id.salir) {
                    finish();
                    return true;
                }
                return super.onOptionsItemSelected(item);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void SiguienteVocales() {
        Ir(MenuVocales.class);
    }

    public void SiguienteAlfa() {
        Ir(MenuAlfabeto.class);
    }

    public void SiguienteNum() {
        Ir(MenuNumeros.class);
    }

    public void SiguienteColores() {
        Ir(MenuColores.class);
    }

    public void SiguienteFrutas() {
        Ir(MenuFrutas.class);
    }

    public void SiguienteAnim() {
        Ir(MenuAnimales.class);
    }

    public void SiguienteSaludos() {
        Ir(MenuSaludos.class);
    }

    public void SiguientePalabras() {
        Ir(MenuPalabras.class);
    }



    private void paraMusica() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.vocales:
                SiguienteVocales();
                break;

            case R.id.alfabeto:
                SiguienteAlfa();
                break;

            case R.id.animales:
                SiguienteAnim();
                break;

            case R.id.colores:
                SiguienteColores();
                break;
            case R.id.numeros:
                SiguienteNum();
                break;

            case R.id.frutas:
                SiguienteFrutas();
                break;

            case R.id.palabras:
                SiguientePalabras();
                break;

            case R.id.saludos:
                SiguienteSaludos();
                break;
        }
    }

    public void Ir(Class clase) {
        if (mediaPlayer != null)
            paraMusica();
        Intent n = new Intent(getApplicationContext(), clase);
        startActivity(n);
        finish();
    }
}
