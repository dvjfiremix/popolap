package com.srll.app.itssat.popolucaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.srll.app.itssat.popolucaapp.Utils.Utils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MenuVocales extends AppCompatActivity {

    ImageView introImageView, memoramaImageview, rompecabezaImageview, rompecabezaCImageview;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_vocales);

        Toolbar toolbar2 = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // instanciamos e iniciamos el Imageview que indica el uso del intro.
        introImageView = findViewById(R.id.imageView15);

        // instanciamos e iniciamos el Imageview que indica el uso del rompecabeza.
        memoramaImageview = findViewById(R.id.imageView17);

        // instanciamos e iniciamos el Imageview que indica el uso del memorama.
        rompecabezaImageview = findViewById(R.id.imageView16);

        rompecabezaCImageview = findViewById(R.id.imageView19);

        // Agregamos un evento clic al Imageview que se encargara de iniciar el intro
        introImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuIntro.class);
                startActivity(i);
            }
        });

        // Agregamos un evento clic al Imageview que se encargara de iniciar el memorama
        memoramaImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuMemoramaVocales.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        // Agregamos un evento clic al Imageview que se encargara de iniciar el memorama
        rompecabezaImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuRompeCabezasActivity.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });

        rompecabezaCImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuRompeCabezasActivity1.class);//MemoramaVocales.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onResume() {
        utils = Utils.getInstance(getApplicationContext());
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        utils.Regresar();
        Intent n = new Intent(getApplicationContext(), MainActivity.class);
        n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(n);
        super.onBackPressed();
    }


}