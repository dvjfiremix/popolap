package com.srll.app.itssat.popolucaapp.Sliders.Fragments;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.srll.app.itssat.popolucaapp.R;

import androidx.fragment.app.Fragment;

public class FragmentVoLargo extends Fragment implements View.OnClickListener {
    ImageView imagen;
    TextView text1, text2;
    FloatingActionButton btn;
    String msj1 = "", msj2 = "";
    int image = R.drawable.animales;
    int audi = R.raw.bienvenida;
    private MediaPlayer media;

    public FragmentVoLargo(String t1, String t2, int img, int audio) {
        msj1 = t1;
        msj2 = t2;
        if (img != 0) {
            image = img;
        }

        if (audio != 0) {
            audi = audio;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.slider_page, container, false);
        imagen = view.findViewById(R.id.imagen);
        text1 = view.findViewById(R.id.texto1);
        text2 = view.findViewById(R.id.texto2);
        text1.setText(msj1);
        text2.setText(msj2);
        Picasso.with(getActivity())
                .load(image)
                .into(imagen);
        btn = view.findViewById(R.id.listen);
        btn.setOnClickListener(this);
        return view;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.listen:
                media = MediaPlayer.create(getActivity(), audi);
                media.start();
                break;
        }
    }

    @Override
    public void onDestroy() {
        if (media != null && media.isPlaying()) {
            media.stop();
            media.reset();
            media.release();
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        if (media != null && media.isPlaying()) {
            media.stop();
            media.reset();
            media.release();
        }
        super.onPause();
    }
}
